/* SPDX-License-Identifier: LGPL-2.1-or-later */
/*
 * Copyright (C) 2019 Red Hat
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gio/gio.h>

#include "service-providers.h"

static void
test_no_carrier_prefix_cb (const char *carrier,
                           const char *vvm_std,
                           const char *dest_num,
                           const char *carrier_prefix,
                           GError     *error,
                           gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_no_error (error);
  g_assert_cmpstr (carrier, ==, "Sophia");
  g_assert_cmpstr (vvm_std, ==, "vvm3");
  g_assert_cmpstr (dest_num, ==, "123456");
  g_assert_null (carrier_prefix);
}

/*
 * The MCC/MNC 345999 exists and has all visual voicemail attributes except for
 * carrier prefix.
 */
static void
test_no_carrier_prefix (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  vvmd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "123123",
                                        test_no_carrier_prefix_cb,
                                        loop);

  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

static void
test_positive_cb (const char *carrier,
                  const char *vvm_std,
                  const char *dest_num,
                  const char *carrier_prefix,
                  GError     *error,
                  gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_no_error (error);
  g_assert_cmpstr (carrier, ==, "Personal");
  g_assert_cmpstr (vvm_std, ==, "cvvm");
  g_assert_cmpstr (dest_num, ==, "098765");
  g_assert_cmpstr (carrier_prefix, ==, "//VVM");
}

/*
 * The MCC/MNC 345999 exists and has all visual voicemail attributes.
 */
static void
test_positive (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  vvmd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "345999",
                                        test_positive_cb,
                                        loop);

  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

static void
test_no_vvm_cb (const char *carrier,
                  const char *vvm_std,
                  const char *dest_num,
                  const char *carrier_prefix,
                  GError     *error,
                  gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_error (error, 1, 1);
}

/*
 * The MCC/MNC 133666 exists, but has no visual voicemail attributes.
 */
static void
test_no_vvm (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  vvmd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "133666",
                                        test_no_vvm_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

static void
test_negative_cb (const char *carrier,
                  const char *vvm_std,
                  const char *dest_num,
                  const char *carrier_prefix,
                  GError     *error,
                  gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_error (error, 1, 1);
}

/*
 * The MCC/MNC 78130 does not exist.
 */
static void
test_negative (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  vvmd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "78130",
                                        test_negative_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

static void
test_nonexistent_cb (const char *carrier,
                     const char *vvm_std,
                     const char *dest_num,
                     const char *carrier_prefix,
                     GError     *error,
                     gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_assert_error (error, G_IO_ERROR, G_IO_ERROR_AGAIN);
  g_main_loop_quit (loop);
}

/*
 * Test to see what happens when you point to the wrong xml file, it won't
 * exist
 */
static void
test_nonexistent (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  vvmd_service_providers_find_settings ("nonexistent.xml",
                                        "13337",
                                        test_nonexistent_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/service-providers/positive", test_positive);
  g_test_add_func ("/service-providers/no_carrier_prefix", test_no_carrier_prefix);
  g_test_add_func ("/service-providers/no_vvm", test_no_vvm);
  g_test_add_func ("/service-providers/negative", test_negative);
  g_test_add_func ("/service-providers/nonexistent", test_nonexistent);

  return g_test_run ();
}
