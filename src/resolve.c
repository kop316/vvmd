/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2024, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "vvm.h"
#include "resolve.h"
#include <arpa/inet.h>
#include <ares.h>
#include <stdio.h>

/* Callback that is called when DNS query is finished */
static void
resolve_callback (void                  *arg,
                  int                    status,
                  int                    timeouts,
                  struct  ares_addrinfo *result)
{
  char **host_ip = (char **)arg;

  DBG ("Result: %s, timeouts: %d", ares_strerror(status), timeouts);

  if (result) {
    struct ares_addrinfo_node *node;
    for (node = result->nodes; node != NULL; node = node->ai_next) {
      char        addr_buf[64] = "";
      const void *ptr          = NULL;

      if (node->ai_family == AF_INET) {
        const struct sockaddr_in *in_addr =
          (const struct sockaddr_in *)((void *)node->ai_addr);
        ptr = &in_addr->sin_addr;
      } else if (node->ai_family == AF_INET6) {
        const struct sockaddr_in6 *in_addr =
          (const struct sockaddr_in6 *)((void *)node->ai_addr);
        ptr = &in_addr->sin6_addr;
      } else {
        continue;
      }

      ares_inet_ntop(node->ai_family, ptr, addr_buf, sizeof(addr_buf));
      if (*addr_buf) {
        /* If you reached here, you found an ip. Don't look for anymore. */
        DBG ("Addr: %s", addr_buf);
        *host_ip = g_strdup (addr_buf);
        break;
      }
    }
  }

  ares_freeaddrinfo(result);
}

/* Shamelessly taken from https://c-ares.org/docs.html#example */
char *
vvm_resolve_resolve_host (const char *host,
                          const char *mailbox_interface,
                          const char *resolvers_ipv4_csv,
                          const char *resolvers_ipv6_csv)
{
  int                         ares_return;
  ares_channel_t             *channel = NULL;
  struct ares_options         options;
  int                         optmask = 0;
  struct ares_addrinfo_hints  hints;
  gchar                      *host_ip = NULL;
  g_autofree gchar           *nsall_csv = NULL;

  DBG ("%s", __func__);

  /* Initialize library */
  ares_library_init(ARES_LIB_INIT_ALL);

  if (!ares_threadsafety()) {
    g_warning ("c-ares not compiled with thread support");
    goto ares_out;
  }

  /* Enable event thread so we don't have to monitor file descriptors */
  memset (&options, 0, sizeof(options));
  optmask      |= ARES_OPT_EVENT_THREAD;
  options.evsys = ARES_EVSYS_DEFAULT;

  /*
   * Initialize channel to run queries, a single channel can accept unlimited
   * queries
   */
  ares_return = ares_init_options(&channel, &options, optmask);
  if (ares_return != ARES_SUCCESS)
    {
      g_warning ("Ares init failed: %s", ares_strerror (ares_return));
      goto ares_out;
    }

  /*
   * ares_set_local_dev () works without root
   * https://github.com/c-ares/c-ares/issues/405
   */
  DBG ("Binding resolver queries to interface %s", mailbox_interface);
  ares_set_local_dev (channel, mailbox_interface);

  if (resolvers_ipv6_csv && *resolvers_ipv6_csv &&
      resolvers_ipv4_csv && *resolvers_ipv4_csv)
    nsall_csv = g_strjoin (",", resolvers_ipv6_csv, resolvers_ipv4_csv, NULL);
  else if (resolvers_ipv6_csv && *resolvers_ipv6_csv)
    nsall_csv = g_strdup (resolvers_ipv6_csv);
  else if (resolvers_ipv4_csv && *resolvers_ipv4_csv)
    nsall_csv = g_strdup (resolvers_ipv4_csv);
  else {
      g_warning ("No active DNS Servers");
      goto ares_out;
    }

  DBG ("All Nameservers: %s", nsall_csv);

  ares_return = ares_set_servers_csv (channel, nsall_csv);
  if (ares_return != ARES_SUCCESS)
    g_warning ("Ares failed to set list of nameservers ('%s'), %s",
               nsall_csv, ares_strerror (ares_return));


  /* Perform an IPv4 and IPv6 request for the provided domain name */
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_flags  = ARES_AI_CANONNAME;
  ares_getaddrinfo(channel, host, NULL, &hints, resolve_callback,
                   (void *)&host_ip);

  /* Wait until no more requests are left to be processed */
  ares_queue_wait_empty(channel, -1);

  if (host_ip == NULL)
    {
      g_warning ("Failed to resolve '%s'", host);
      goto ares_out;
    }

 ares_out:
  ares_destroy (channel);
  ares_library_cleanup();
  return host_ip;
}

int
vvm_resolve_ip_version(const char *src)
{
  char buf[INET6_ADDRSTRLEN];

  if (inet_pton(AF_INET, src, buf))
    return 4;
  else if (inet_pton(AF_INET6, src, buf))
    return 6;

  return -1;
}
